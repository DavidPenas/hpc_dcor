#include <stdio.h>
#include <stdlib.h>

int main () {
   int i, n;
   time_t t;
   FILE *fp;
   
   n = 1e5;
   
   /* Intializes random number generator */
   srand((unsigned) time(&t));
   fp=fopen("random_numbers.txt", "w");

   /* Print 5 random numbers from 0 to 49 */
   for( i = 0 ; i < n ; i++ ) {
      fprintf(fp, "%lf\n", ((rand() % 10000) / 10000.0));
   }
   
   return(0);
}
