# README #

## Install ##

Software requirements to have installed in a Linux system:

* R version 3.6.2

* Intel C++ and Fortran Compilers 2016

* Intel MKL

* openMPI 2.1.1

* bzip 1.0.6

* xz 5.2.4

* pcre 8.42

* curl 7.61.1

Have the following environment variables defined:

* R_LIB=//R_root_path//lib64/R/lib

* R_HOME=//R_root_path//lib64/R

* LD_LIBRARY_PATH=$(R_LIB):$LD_LIBRARY_PATH

Include the following directories in the Makefile:

* R_LIB=//R_root_path//lib64/R/lib

* R_INCLUDE=//R_root_path//lib64/R/include 

And finally, execute the following commands in the main program path:

* make clean

* make

If everything has been compiled correctly, an executable will appear in the bin folder.

## Scheme ##

* **bin** -> Executable path.
* **src** -> Main source code files implemented in C.
* **input_mat** -> Input matrices Matrix_X_SCZ.dat  and Matrix_Y_SCZ.dat are stored here. 
* **R/input** -> It contains a file with some input options to be completed by users.
* **R/method** -> This dir contains R codes to implement input and output methods.
* **output** -> The generated results are saved in this path.
* **script1.sh** and **script2.sh** ->  These scripts correspond to an example of sending a parallel work in an SLURM queue system.



## Use

#### Step 1: $ mpirun -np NUM_PROCESSORS ./bin/program 0 

When the execution was finished, a new output file called "DATE_START_DATE_END_pvalX.out" will be generated (find it in **output** directory).

#### Step 2: $ mpirun -np NUM_PROCESSORS ./bin/program 1 

When the execution was finished, a new output file called "DATE_START_DATE_END__pvalY.out" will be generated (find it in **output** directory).

#### Step 3: $ ./bin/program 2 output/DATE_START_DATE_END_pvalX.out output/DATE_START_DATE_END_pvalY.out

In the end, this option processes the results of the two previous parts of the test, writing a new file with the definitive results, named: "TOTAL_DATE_START_DATE_END_log.out".
## References ##

Fernando Castro-Prado, Javier Costas, Wenceslao Gonzalez-Manteiga, David R Penas (2020). Scanning for the missing heritability of complex disease by means of distance correlation (preparing).