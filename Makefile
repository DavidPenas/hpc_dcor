R_LIB=/home/usc/ei/drp/R/lib64/R/lib
R_INCLUDE=/home/usc/ei/drp/R/lib64/R/include

#####################################################################

PARALLEL=-lmpi
CLIB+=  -O3 -ipo -xHost -fPIC -m64 -limf -lifcore -axAVX 
LIB+= -lm -lpthread -lR -lRblas  -lRlapack
LIB+=-L$(R_LIB)
LIB+= -lmkl_intel_ilp64 -lmkl_core -lmkl_sequential -lpthread -lm  -lmkl_rt -lmkl_avx2 -lmkl_def -lmkl_vml_avx -lmkl_vml_avx2 -lpcre -llzma
INC+=-I$(R_INCLUDE)
INC+=$(IMPI)
SRC+=$(wildcard ./src/*.c)

#CC:=mpicc $(CLIB) $(PARALLEL) #-DREPRODUCIBLE #-DDETERMINISTIC
CC:=mpicc $(CLIB) $(PARALLEL) #-DDETERMINISTIC
PROG := bin/program

OBJFILES := $(SRC:.c=.o)
DEPFILES := $(SRC:.c=.d)

 %.o: %.c
	$(CC)  -c $< -o $@  $(LIB) $(INC) 

$(PROG) : $(OBJFILES)
	$(LINK.o) -o $(PROG) $(OBJFILES) $(LIB)

clean :
	rm -f $(PROG) $(OBJFILES) $(DEPFILES)
