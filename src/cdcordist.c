#include <stdio.h>
#include <stdlib.h>
#include <R.h>
#include <Rinternals.h>
#include <mkl.h>
#include <mkl_vml.h>
#include <mkl_vsl.h>
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#include <time.h>
#include <omp.h>
#include <mpi.h>
#include <immintrin.h>
#include "cdcordist.h"
#define max_size_file_random 100000

const int alignment = 32;
const int incOne = 1;


void create_index_vec(int *x, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
	int size;
        __m256i u;

		size=8; 
        //mod_int = (*n_size % size);
        for (i = 0; i < *n_size_real8; i++) {
                 u = _mm256_set_epi32 (i*size+7, i*size+6, i*size+5, i*size+4, i*size+3, i*size+2, i*size+1, i*size); 
                 _mm256_store_si256 ((__m256i*) &x[i*size], u ); // store back
        }
        for (i = *n_size_real; i < *n_size; i++) x[i] = i;
}

void reorder_vector(float *x, float *out, int *index, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
        //int n_size_real;
        __m256  x_vec;

        //mod_int = (*n_size % 8);
        //n_size_real=*n_size-mod_int;
        for (i = 0; i < *n_size_real8; i++) {
                 x_vec = _mm256_set_ps(x[index[i*8+7]],x[index[i*8+6]],x[index[i*8+5]],x[index[i*8+4]],x[index[i*8+3]],x[index[i*8+2]],x[index[i*8+1]],x[index[i*8]]);
                 _mm256_store_ps(out+i*8, x_vec); // store back
        }
        for (i = *n_size_real; i < *n_size; i++) out[i] = x[index[i]];
}


void set_zero(float *x, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
        __m256 x_vec;

        for (i = 0; i < *n_size_real8; i++) {
                 x_vec = _mm256_setzero_ps(); // add index
                 _mm256_store_ps(x+i*8, x_vec); // store back
        }
        for (i = *n_size_real; i < *n_size; i++) x[i] = 0.0;
}


void add_vec_scalar(float *x, float *scalar, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
        __m256 index, x_vec;

	index = _mm256_set_ps(*scalar, *scalar, *scalar, *scalar, *scalar, *scalar, *scalar, *scalar);
        //mod_int = (*n_size % 8);
        //n_size_real=*n_size-mod_int;
	for (i = 0; i < *n_size_real8; i++) {
		 x_vec = _mm256_load_ps(x+i*8); // load 4 floats
		 x_vec = _mm256_add_ps(x_vec, index); // add index
		 _mm256_store_ps(x+i*8, x_vec); // store back
	}
	for (i = *n_size_real; i < *n_size; i++) x[i] = x[i] + *scalar;
}


void reduction_vec(float *x, float *out, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
//        int n_size_real;
	float res[8];
        __m256 x_vec1, x_vec2;

        x_vec2 = _mm256_setzero_ps(); // add index
//        _mm256_store_ps(res+i*8, x_vec1);
	

        //mod_int = (n_size % 8);
//        n_size_real=n_size-mod_int;

        for (i = 0; i < *n_size_real8; i++) {
                 x_vec1 = _mm256_load_ps(x+i*8); // load 8 floats
                 x_vec2 = _mm256_add_ps(x_vec1, x_vec2); // add index
        }

	*out=0.0;		
        _mm256_store_ps(res, x_vec2); // store back
	for (i=0;i<8;i++) *out=*out+res[i];	
        for (i = *n_size_real; i < *n_size; i++) *out = x[i] + *out;
}


void return_col_row(float *x, float *out1, float *out2, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
        float res[8];
        __m256 x_vec1, x_vec2, x_vec3;
        x_vec2 = _mm256_setzero_ps();

        for (i = 0; i < *n_size_real8; i++) {
                 x_vec1 = _mm256_load_ps(x+i*8);
                 x_vec3 = _mm256_load_ps(out1+i*8);
                 x_vec3 = _mm256_add_ps(x_vec3, x_vec1);
                 x_vec2 = _mm256_add_ps(x_vec2, x_vec1);
                 _mm256_store_ps(out1+i*8, x_vec3);
        }

        *out2=0.0;
        _mm256_store_ps(res, x_vec2);
        for (i=0;i<8;i++)                        *out2    = *out2   + res[i];
        for (i = *n_size_real; i < *n_size; i++) *out2    = x[i]    + *out2;
        for (i = *n_size_real; i < *n_size; i++)  out1[i] = out1[i] + x[i];

}



void mul_vec_scalar(float *x, float *scalar,int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
//        int n_size_real;
        __m256 index, x_vec;

        index = _mm256_set_ps(*scalar, *scalar, *scalar, *scalar, *scalar, *scalar, *scalar, *scalar);
        //mod_int = (*n_size % 8);
//        n_size_real=(*n_size-mod_int)/8;
        for (i = 0; i < *n_size_real8; i++) {
                 x_vec = _mm256_load_ps(x+i*8); // load 4 floats
                 x_vec = _mm256_mul_ps(x_vec, index); // add index
                 _mm256_store_ps(x+i*8, x_vec); // store back
        }
        for (i = *n_size_real; i < *n_size; i++) x[i] = x[i] * (*scalar);
}

void add_vec_colum(float *x, float *out, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
        int size;
        __m256  x_vec1, x_vec2;

	size=8;
        //mod_int = (*n_size % size);
        //n_size_real=*n_size-mod_int;
        for (i = 0; i < *n_size_real8; i++) {
                 x_vec1 = _mm256_load_ps(x+i*size); // load 8 floats
                 x_vec2 = _mm256_load_ps(out+i*size); // load 8 floats
		 x_vec1 = _mm256_add_ps(x_vec1, x_vec2);
		 _mm256_store_ps(out+i*size, x_vec1);
        }
        for (i = *n_size_real; i < *n_size; i++) out[i] = out[i] + x[i];
}

void add_vec_vec(float *x, float *y, float *out, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
        int size=8;
        __m256  x_vec1, x_vec2;

        for (i = 0; i < *n_size_real8; i++) {
                 x_vec1 = _mm256_load_ps(x+i*size); // load 4 floats
                 x_vec2 = _mm256_load_ps(y+i*size); // load 4 floats
                 x_vec1 = _mm256_add_ps(x_vec1, x_vec2);
                 _mm256_store_ps(out+i*size, x_vec1);
        }
        for (i = *n_size_real; i < *n_size; i++) out[i] = x[i] + y[i];
}



void mul_vec_vec(float *x, float *y, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
        int size;
        __m256  x_vec1, x_vec2;
	size=8;
        for (i = 0; i < *n_size_real8; i++) {
                 x_vec1 = _mm256_load_ps(x+i*size); 
                 x_vec2 = _mm256_load_ps(y+i*size);
                 x_vec1 = _mm256_mul_ps(x_vec1, x_vec2);
                 _mm256_store_ps(x+i*size, x_vec1);
        }
        for (i = *n_size_real; i < *n_size; i++) x[i] = x[i] * y[i];
}


void add_vec_scalar2(float *x, float scalar, float *out, int *n_size_real8, int *n_size_real, int *n_size) {
        int i;
//        int n_size_real;
        __m256 index, incr, x_vec;

        index = _mm256_set_ps(scalar, scalar, scalar, scalar, scalar, scalar, scalar, scalar);
        //mod_int = (n_size % 8);
//        n_size_real=n_size-mod_int;
        for (i = 0; i < *n_size_real8; i++) {
                 x_vec = _mm256_load_ps(x+i*8); // load 4 floats
                 x_vec = _mm256_add_ps(x_vec, index); // add index
                 _mm256_store_ps(out+i*8, x_vec); // store back
        }
        for (i = *n_size_real; i < *n_size; i++) out[i] = x[i] + scalar;
}


/*
dcordist<-function(distX,distY,n){
  c_distX<-distX-matrix(colMeans(distX),n,n,byrow=T)-matrix(rowMeans(distX),n,n)+mean(distX)
  c_distY<-distY-matrix(colMeans(distY),n,n,byrow=T)-matrix(rowMeans(distY),n,n)+mean(distY)
  c_dist = c_distX*c_distY 
  meanresult=mean(c_dist)
  return(meanresult)
}
*/
void calc_matrix_dist(float *distX_c, float *aux_distX_c2, commonvars	*cvars) {
  int i,j,k;
  float value_avg;
  float *aux_distX_c1;
  float *vect_aux1, *vect_aux2;
  
  vect_aux1= (float*) mkl_malloc(cvars->n_size * sizeof(float), alignment);
  vect_aux2= (float*) mkl_malloc(cvars->n_size * sizeof(float), alignment);

	  
  // aux_distX_c1 <-- matrix(colMeans(distX),n,n,byrow=T)
  set_zero(vect_aux1, &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));

  for (i=0;i<cvars->n_size;i++) {
	return_col_row(&distX_c[cvars->n_size*i], vect_aux1, &vect_aux2[i], &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
  }
//  mul_vec_scalar(vect_aux1, &(cvars->neg_n_size),  &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
  cblas_sscal (cvars->n_size, cvars->neg_n_size, vect_aux1, incOne);
  reduction_vec(vect_aux1, &value_avg,   &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
  value_avg = value_avg * cvars->neg_n_size;

  // aux_distX_c2 <-- matrix(rowMeans(distX),n,n)+mean(distX)
//  mul_vec_scalar(vect_aux2, &(cvars->neg_n_size),  &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
  cblas_sscal (cvars->n_size, cvars->neg_n_size, vect_aux2, incOne);
  add_vec_scalar(vect_aux2, &value_avg, &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
 
  //  aux_distX_c3 <-- distX - aux_distX_c1 - aux_distX_c2
  for (i=0;i<cvars->n_size;i++) {
	  add_vec_scalar2(vect_aux2,vect_aux1[i],&aux_distX_c2[cvars->n_size*i], &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
	  add_vec_vec(&distX_c[cvars->n_size*i], &aux_distX_c2[cvars->n_size*i], &aux_distX_c2[cvars->n_size*i], &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
  }
//  add_vec_vec(distX_c, aux_distX_c2, aux_distX_c2, &(cvars->n_size_real28), &(cvars->n_size_real2), &(cvars->n_size2));


  mkl_free(vect_aux1);
  vect_aux1=NULL;
  mkl_free(vect_aux2);
  vect_aux2=NULL;

}


// FisherYates
void FisherYates_rep(float *distX_c, float *aux_distX, float *random_stream1, commonvars	*cvars) { 
     int i, ri, rj, j;
     int k, tmp;
     int *vector_int;


     vector_int = (int *) mkl_malloc(cvars->n_size * sizeof(int), alignment);
     create_index_vec(vector_int, &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));

     for (i = cvars->n_size; i > 0; i--) {
         j = (int) (random_stream1[i-1] * ((double) i))+1;
	 tmp = vector_int[j-1];
         vector_int[j-1] = vector_int[i-1];
         vector_int[i-1] = tmp;
     }

     for (i=0;i<cvars->n_size;i++) {
        reorder_vector(&distX_c[cvars->n_size*vector_int[i]], &aux_distX[cvars->n_size*i], vector_int, &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
     } 
     free(vector_int);
     vector_int=NULL;
}

void FisherYates(float *distX_c, float *aux_distX, VSLStreamStatePtr *str, commonvars	*cvars) {
     int i, ri, rj, j;
     int k,tmp;
     int *vector_int;
     float UB=1;
     float LB=0;
     float *random_stream1;

     vector_int = (int *) mkl_malloc(cvars->n_size * sizeof(int), alignment);
     create_index_vec(vector_int, &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));

     random_stream1 = (float *) mkl_malloc(cvars->n_size*sizeof(float), alignment);
     vsRngUniform( VSL_RNG_METHOD_UNIFORM_STD, *str, cvars->n_size, random_stream1, LB, UB );

     for (i = cvars->n_size; i > 0; i--) {
         j = (int) (random_stream1[i-1] * ((float) i))+1;
         tmp = vector_int[j-1];
         vector_int[j-1] = vector_int[i-1];
         vector_int[i-1] = tmp;
     }

     for (i=0;i<cvars->n_size;i++) {
        reorder_vector(&distX_c[cvars->n_size*vector_int[i]], &aux_distX[cvars->n_size*i], vector_int, &(cvars->n_size_real8), &(cvars->n_size_real), &(cvars->n_size));
     }

     mkl_free(random_stream1);
     random_stream1=NULL;
     free(vector_int);
     vector_int=NULL;
}


void FisherYates2(float *distX_c, float *aux_distX, float *random_stream1, int *vector_int, commonvars *cvars) {
     int i,j,k,tmp,ri,rj;

     for (i = cvars->n_size; i > 0; i--) {
         j = ((int) (random_stream1[i-1] * ((float) i)))+1;
         tmp = vector_int[j-1];
         vector_int[j-1] = vector_int[i-1];
         vector_int[i-1] = tmp;
     }

     for (i=0;i<cvars->n_size;i++) {
         ri = vector_int[i];
         for (j=0;j<cvars->n_size;j++) {
                rj = vector_int[j];
                aux_distX[i*cvars->n_size+j]=distX_c[ri*cvars->n_size+rj];
         }
     }

}


void FisherYates_old(float *distX_c, float *aux_distX, VSLStreamStatePtr *str, commonvars       *cvars) {
     int i, ri, rj, j;
     float UB=1;
     float LB=0;
     int k,tmp;
     int *vector_int;
     float *random_stream1;

     vector_int = (int *) mkl_malloc(cvars->n_size * sizeof(int), alignment);
     for (k=0;k<cvars->n_size;k++){
         vector_int[k]=k;
     }

     random_stream1 = (float *) mkl_malloc(cvars->n_size*sizeof(float), alignment);
     vsRngUniform( VSL_RNG_METHOD_UNIFORM_STD, *str, cvars->n_size, random_stream1, LB, UB );

     for (i = cvars->n_size; i > 0; i--) {
         j = (int) (random_stream1[i-1] * ((double) i))+1;
         tmp = vector_int[j-1];
         vector_int[j-1] = vector_int[i-1];
         vector_int[i-1] = tmp;
     }

     for (i=0;i<cvars->n_size;i++) {
         ri = vector_int[i];
         for (j=0;j<cvars->n_size;j++) {
                rj = vector_int[j];
                aux_distX[i*cvars->n_size+j]=distX_c[ri*cvars->n_size+rj];
         }
     }

     mkl_free(random_stream1);
     random_stream1=NULL;
     free(vector_int);
     vector_int=NULL;
}




// Samples with replacement
void Samples_with_replacement_rep(float *distX_c, float *aux_distX, float *random_stream1, commonvars	*cvars) {
     int i, ri, rj, j;
     int k;
     float UB=cvars->n_size;
     float LB=0;

     for (k=0;k<cvars->n_size;k++){
        random_stream1[k] =  random_stream1[k] * UB;
     }

     for (i=0;i<cvars->n_size;i++) {
         ri = (int) random_stream1[i];
         for (j=0;j<cvars->n_size;j++) {
                rj = (int) random_stream1[j];
                aux_distX[i*cvars->n_size+j]=distX_c[ri*cvars->n_size+rj];
         }
     }

}

// Samples with replacement
void Samples_with_replacement(float *distX_c, float *aux_distX, VSLStreamStatePtr *str, commonvars	*cvars) {
     int i, ri, rj, j;
     float UB=cvars->n_size;
     float LB=0;
     float *random_stream1;

     random_stream1 = (float *) mkl_malloc(cvars->n_size*sizeof(float), alignment);
     vsRngUniform( VSL_RNG_METHOD_UNIFORM_STD, *str, cvars->n_size, random_stream1, LB, UB );

     for (i=0;i<cvars->n_size;i++) {
         ri = (int) random_stream1[i];
         for (j=0;j<cvars->n_size;j++) {
                rj = (int) random_stream1[j];
                aux_distX[i*cvars->n_size+j]=distX_c[ri*cvars->n_size+rj];
         }
     }

     mkl_free(random_stream1);
     random_stream1=NULL;
}


// dcordist_r
float dcordist_c(float *distX_c, float *distY_c, commonvars	*cvars) {
  int i,j;
  float average;
  float *aux_distX, *aux_distY;

  aux_distX= (float*) mkl_malloc(cvars->n_size2 * sizeof(float), alignment);
  aux_distY= (float*) mkl_malloc(cvars->n_size2 * sizeof(float), alignment);

//  c_distX<-distX-matrix(colMeans(distX),n,n,byrow=T)-matrix(rowMeans(distX),n,n)+mean(distX)
  calc_matrix_dist(distX_c, aux_distX, cvars);

//  c_distY<-distY-matrix(colMeans(distY),n,n,byrow=T)-matrix(rowMeans(distY),n,n)+mean(distY)
  calc_matrix_dist(distY_c, aux_distY, cvars);
  cblas_scopy(cvars->n_size2, aux_distY, incOne, distY_c, incOne);

//  c_dist = c_distX*c_distY
//  meanresult=mean(c_dist)  
  average=0.0;
  mul_vec_vec(aux_distX, aux_distY, &(cvars->n_size_real28), &(cvars->n_size_real2), &(cvars->n_size2));
  reduction_vec(aux_distX, &average, &(cvars->n_size_real28), &(cvars->n_size_real2), &(cvars->n_size2));

  average = average * cvars->inv_n_size2;

  mkl_free(aux_distX);
  aux_distX = NULL;
  mkl_free(aux_distY);
  aux_distY = NULL;
  mkl_free_buffers();
  return average;
}

float dcordist_c2(float *distX_c, float *distY_c, commonvars	*cvars) {
  int i,j;
  float average;
  float *aux_distX ;

  aux_distX= (float*) mkl_malloc(cvars->n_size2 * sizeof(float), alignment);

//  c_distX<-distX-matrix(colMeans(distX),n,n,byrow=T)-matrix(rowMeans(distX),n,n)+mean(distX)
  calc_matrix_dist(distX_c, aux_distX, cvars);
  average=0.0;
  mul_vec_vec(aux_distX, distY_c, &(cvars->n_size_real28), &(cvars->n_size_real2), &(cvars->n_size2));

  reduction_vec(aux_distX, &average, &(cvars->n_size_real28), &(cvars->n_size_real2), &(cvars->n_size2));
  average= average * cvars->inv_n_size2;

  mkl_free(aux_distX);
  aux_distX = NULL;
  mkl_free_buffers();
  return average;
}


float function_equilat(int A, int B) {
//      d<-function(x,y) as.numeric(x!=y)
        if (A!=B) return (float) 1.0;
        else return (float) 0.0;
}

float function_one_equal_two(int A, int B) {
//	d<-function(x,y) any(c(x,y)==0)*any(c(x,y)>0)
	float cond1=0;
	float cond2=0;
	if ((A==0)||(B==0)) cond1 = 1.0;
        else cond1 = 0.0;
	
        if ((A>0)||(B>0)) cond2 = 1.0;
        else cond2 = 0.0;	

	return cond1*cond2;
}

float function_zero_equal_two(int A, int B) {
//      d<-function(x,y) any(c(x,y)==0)*any(c(x,y)>0)
        float cond1=0;
        float cond2=0;
        if ((A==1)||(B==1)) cond1 = 1.0;
        else cond1 = 0.0;

        if ((A!=1)||(B!=1)) cond2 = 1.0;
        else cond2 = 0.0;

        return cond1*cond2;
}

float function_zero_equal_one(int A, int B) {
//	d<-function(x,y) any(c(x,y)==2)*any(c(x,y)!=2)
        float cond1=0;
        float cond2=0;
        if ((A==2)||(B==2)) cond1 = 1.0;
        else cond1 = 0.0;

        if ((A!=2)||(B!=2)) cond2 = 1.0;
        else cond2 = 0.0;

        return cond1*cond2;
}

float function_mean_Rstar_point(float *Rstar, int size_R, float point) {
	int i;
	float avg;
	float epsilon;

	epsilon=0.00001;
	avg=0.0;
	for (i=0;i<size_R;i++) {
		if (( Rstar[i] == point ) || ( Rstar[i] < (point+epsilon))) {
			avg=avg+1.0;
		}
		//printf("Rstar[i] %lf <= point %lf = avg %lf\n", Rstar[i], point, avg);
	}

	return avg / ((float) size_R);
}

int return_min_function(float *vector, int size_vector) {
	int i, index=0;
	float aux=FLT_MAX;
	index=1;
	for (i=0;i<size_vector;i++) {
		
		if (vector[i] < aux) {
			aux=vector[i];
			index=i;
		}	
	}

	return index+1;
}


void calcijsize(int counter, int *i, int *j, int size) {
        int ii, jj;
	int temp, exit;
        ii = size-1;
        *i=-1;
	exit=0;
	temp=counter;
	jj=-1;
        while (exit==0) {
                *i=*i+1;
                temp=temp-(ii);
		if (temp < 0){ 
			exit=1;
		} else jj=temp;
                ii=ii-1;
        }
	if (*i == 0) {
        	*j=counter-*i + 1;
	}
	else {
		*j=*i + jj + 1;
	}
}




void testdcor(int NPROC, int id, SEXP XmatR, SEXP Xmat_sizexR, SEXP Xmat_sizeyR, SEXP silentR, SEXP distanceR, SEXP B1R, SEXP NR, SEXP alphaR, int *counterp, VSLStreamStatePtr *str, SEXP *RechazX) {
     int z,k;
     int counter;
     int SIZE;
     int ii, jj;
     int zz;
     int pC;
     int silentC;
     int B1C;
     int NC;
     int est_counter;
     commonvars	cvars;
     int    i,j;
     int    distanceC;
     int counterR;
     float alphaC;
     float R,that;
     double time1, time2;
     MPI_Status status;
     const char *option1, *option2;
     int *counterR_matrix;
     int   *RechazX_dist;
     float *RechazX_dist_value;
     float  (*function_distance)(int,int);
     int    *vectorX, *vectorY;
     float  *Rstar, *R_aux;
     float  *distX, *distY;
     float  *aux_distX_new;
     float ***randomM;
     int *XmatC;
     double *RechazX_pointer, aux;
     int *vector_int;
     float UB=1;
     float LB=0;
     float *random_stream1;

#ifdef DETERMINISTIC
     int counter_matrix;
     float *random_matrix;
     FILE *fp;
     printf("DETERMINISTIC\n");
     if ((fp = fopen ("deterministic/random_numbers.txt", "r")) == NULL) {
         fprintf (stderr, "\nError openning file function.\n");
         exit(0);
     }
 
     ii=0;
     random_matrix = (float *) mkl_malloc( max_size_file_random * sizeof( float ), alignment);
     while ( !feof (fp) )
     {
         fscanf(fp, "%f\n", &random_matrix[ii]); 
	 ii++;
     }
     counter_matrix=0;
#endif
	
     if (id==0) {
     	cvars.n_size = asInteger(Xmat_sizexR);
     	pC = asInteger(Xmat_sizeyR);
     	silentC = asInteger(silentR);
     	B1C = asInteger(B1R);
     	XmatC  = (int *) INTEGER(XmatR);
     	distanceC = asInteger(distanceR);
     	NC = asInteger(NR);
     	alphaC = (float) asReal(alphaR);
	*RechazX = PROTECT(allocMatrix(REALSXP, pC, pC));
	RechazX_pointer = REAL(*RechazX);
     	*counterp++;
     }

     time1 = MPI_Wtime();
     MPI_Bcast(&cvars.n_size,1,MPI_INT,0,MPI_COMM_WORLD);
     MPI_Bcast(&pC,1,MPI_INT,0,MPI_COMM_WORLD);
     MPI_Bcast(&silentC,1,MPI_INT,0,MPI_COMM_WORLD);
     MPI_Bcast(&B1C,1,MPI_INT,0,MPI_COMM_WORLD);
     if (id != 0) XmatC= (int *) mkl_malloc(cvars.n_size*pC * sizeof(int), alignment); 
     MPI_Bcast(XmatC, cvars.n_size*pC ,MPI_INT,0,MPI_COMM_WORLD);
     MPI_Bcast(&distanceC,1,MPI_INT,0,MPI_COMM_WORLD);
     MPI_Bcast(&NC,1,MPI_INT,0,MPI_COMM_WORLD);
     MPI_Bcast(&alphaC,1,MPI_FLOAT,0,MPI_COMM_WORLD);

     cvars.n_size2      = cvars.n_size*cvars.n_size;
     cvars.inv_n_size   = 1 / (float) cvars.n_size;
     cvars.inv_n_size2  = 1 / (float) cvars.n_size2;
     cvars.neg_n_size   = (float) (-1.0)*cvars.inv_n_size;
     cvars.n_size_real  = cvars.n_size -(cvars.n_size  % 8);
     cvars.n_size_real2 = cvars.n_size2-(cvars.n_size2 % 8);
     cvars.n_size_real8 = cvars.n_size_real/8;
     cvars.n_size_real28= cvars.n_size_real2/8;

     vectorX = (int *)   mkl_malloc(cvars.n_size*sizeof(int), alignment);
     vectorY = (int *)   mkl_malloc(cvars.n_size*sizeof(int), alignment);
     Rstar   = (float *) mkl_malloc(B1C*sizeof(float), alignment);
     R_aux   = (float *) mkl_malloc(NC*sizeof(float), alignment);

     vector_int = (int *) mkl_malloc(cvars.n_size * sizeof(int), alignment);
     random_stream1 = (float *) mkl_malloc(cvars.n_size *sizeof(float), alignment);

     SIZE=(pC*pC-pC)/2;
     int dim_s, mod;
     dim_s = floor(SIZE/NPROC);
     mod = SIZE % NPROC;
     if (id < mod) dim_s=dim_s+1;

     RechazX_dist       = (int *) mkl_malloc   ( dim_s *sizeof(int)  , alignment);
     RechazX_dist_value = (float *) mkl_malloc ( dim_s *sizeof(float), alignment);

     aux_distX_new = (float*) mkl_malloc(cvars.n_size*cvars.n_size*sizeof(float), alignment);
     distX=(float *) mkl_malloc(cvars.n_size*cvars.n_size*sizeof(float), alignment);
     distY=(float *) mkl_malloc(cvars.n_size*cvars.n_size*sizeof(float), alignment);
     counterR_matrix 	= (int *) mkl_malloc(NPROC*sizeof(int), alignment);

     if (distanceC==0) function_distance=&function_equilat;
     else if (distanceC==1) function_distance=&function_one_equal_two;
     else if (distanceC==2) function_distance=&function_zero_equal_two;
     else if (distanceC==3) function_distance=&function_zero_equal_one;
     else function_distance=&function_equilat;

//   GENERATING RANDOM NUMBERS
#ifdef DETERMINISTIC
     printf("generating random numbers [SIZE=%d B1C=%d n_size=%d]\n",SIZE,B1C,cvars.n_size);
     randomM = (float ***) malloc( SIZE*sizeof(float **));
     for (ii=0;ii<SIZE;ii++) {
        printf("    **allocating matrix %d (%d)\n", ii, SIZE);
        randomM[ii] = (float **) malloc( B1C*sizeof(float *));
        for (jj=0;jj<B1C;jj++){
                randomM[ii][jj] = (float *) malloc(cvars.n_size * sizeof(float));
        }
     }
     for (ii=0;ii<SIZE;ii++) {
        printf("    **generating matrix %d (%d)\n", ii, SIZE);
        for (jj=0;jj<B1C;jj++){
		for  (zz=(cvars.n_size-1);zz>=0;zz-- ){
                        randomM[ii][jj][zz] = random_matrix[counter_matrix];
                        counter_matrix++;
                        if (counter_matrix == max_size_file_random) {
				counter_matrix=0;
			}
                }
	}
     }
#elif REPRODUCIBLE
     printf("REPRODUCIBLE\n"); 
     printf("generating random numbers [SIZE=%d B1C=%d n_size=%d]\n",SIZE,B1C,cvars.n_size);
     randomM = (float ***) malloc( SIZE*sizeof(float **));
     for (ii=0;ii<SIZE;ii++) {
        printf("    **allocating matrix %d (%d)\n", ii, SIZE);
        randomM[ii] = (float **) malloc( B1C*sizeof(float *));
        for (jj=0;jj<B1C;jj++){
                randomM[ii][jj] = (float *) malloc(cvars.n_size * sizeof(float));
        }
     }
     for (ii=0;ii<SIZE;ii++) {
	printf("    **generating matrix %d (%d)\n", ii, SIZE);
        for (jj=0;jj<B1C;jj++){
	        vsRngUniform( VSL_RNG_METHOD_UNIFORM_STD, *str, cvars.n_size, randomM[ii][jj], 0, 1.0 );
        }
     }	
#endif

     time2 = MPI_Wtime();
     printf("Comenzando el bucle de los CASOS ID[%d]. Current time [%lfs]. SIZE %d\n",id,time2-time1, SIZE);

     counterR=1;
     est_counter=1;

     MPI_Barrier(MPI_COMM_WORLD);

    
     for (counter=0;counter<SIZE;counter++) {
	if ((counter % NPROC) == id ) {
           //printf("id %d - counter %d - NPROC %d\n", id , counter, NPROC);
           calcijsize(counter, &i, &j, pC);
           for (z=0;z<cvars.n_size;z++) vectorX[z]=XmatC[i*cvars.n_size+z];
           for (z=0;z<cvars.n_size;z++) vectorY[z]=XmatC[j*cvars.n_size+z];

           //build distX distY
           for (z=0;z<cvars.n_size;z++) {
                for (k=0;k<cvars.n_size;k++) {
                       distX[k*cvars.n_size+z]=function_distance(vectorX[z],vectorX[k]);
                       distY[k*cvars.n_size+z]=function_distance(vectorY[z],vectorY[k]);
                } 
           }

	   R=dcordist_c(distX, distY, &cvars);

	   for (z=0;z<B1C;z++) {
#ifdef DETERMINISTIC
	      FisherYates_rep(distX,aux_distX_new,randomM[counter][z], &cvars);
#elif REPRODUCIBLE
              FisherYates_rep(distX,aux_distX_new,randomM[counter][z], &cvars);
#else
              create_index_vec(vector_int, &(cvars.n_size_real8), &(cvars.n_size_real), &(cvars.n_size));
              vsRngUniform( VSL_RNG_METHOD_UNIFORM_STD, *str, cvars.n_size, random_stream1, LB, UB );	
              FisherYates2(distX, aux_distX_new, random_stream1, vector_int, &cvars);
#endif
	      Rstar[z] = dcordist_c2(aux_distX_new, distY, &cvars);
	   }

           RechazX_dist_value[counterR-1]= 1 - function_mean_Rstar_point(Rstar,B1C,R);
           RechazX_dist[counterR-1]=counter;
           counterR++;

          if ((!silentC)) {
                 time2 = MPI_Wtime();
                 if (id == 0) printf("[%d] i=%d; j=%d; [%dx%d]; current time [%f s]; expected time [%f s]\n",id, i+1, j+1, pC, pC, time2-time1, ((double) SIZE/NPROC)*(time2-time1)/((double) est_counter ));
                 est_counter++;
          }
	} 
     }


     if (id == 0) printf("MPI GATHER %d %d \n", counter, SIZE);
		// Gathered ditributed results
		counterR--;
     if (NPROC != 1) {
		MPI_Gather(&counterR, 1, MPI_INT, counterR_matrix, 1, MPI_INT, 0, MPI_COMM_WORLD);
     }  
     else counterR_matrix[0] = counterR;
 
     if (id == 0) {
       // set in the results zeros by default
       for (counter=0;counter<(pC*pC);counter++) {
                        i = floor(counter / pC);
                        j = counter % pC;
                        REAL(*RechazX)[i*pC+j]= 0.0;
       }
	// foreach processor set 1 values
	for (ii=0;ii<NPROC;ii++) {
		if (ii != 0) {
	            RechazX_dist = mkl_realloc(RechazX_dist, counterR_matrix[ii] * sizeof(int));
		    MPI_Recv(RechazX_dist, counterR_matrix[ii], MPI_INT, ii, 100, MPI_COMM_WORLD, &status);
	            RechazX_dist_value = mkl_realloc(RechazX_dist_value, counterR_matrix[ii] * sizeof(float));
	            MPI_Recv(RechazX_dist_value, counterR_matrix[ii], MPI_FLOAT, ii, 100, MPI_COMM_WORLD, &status);			
 		}
		for (counter=0;counter<counterR_matrix[ii];counter++) {
	                calcijsize(RechazX_dist[counter], &i, &j, pC);
			RechazX_pointer[i*pC+j] = RechazX_dist_value[counter];
     		}
       }
	
     }
     else {
   	    	MPI_Send(RechazX_dist, counterR, MPI_INT, 0, 100, MPI_COMM_WORLD);
        	MPI_Send(RechazX_dist_value, counterR, MPI_FLOAT, 0, 100, MPI_COMM_WORLD);
     }


#ifdef DETERMINISTIC
     if (randomM!=NULL){
        for (ii=0;ii<SIZE;ii++) {
                for (jj=0;jj<B1C;jj++){
                        free(randomM[ii][jj]);
                        randomM[ii][jj]=NULL;
                }
        }
        for (ii=0;ii<SIZE;ii++) {
                free(randomM[ii]);
                randomM[ii]=NULL;
        }
        free(randomM);
        randomM=NULL;
     }
     if (random_matrix!=NULL){
        mkl_free(random_matrix);
        random_matrix=NULL;
     }
#endif
     mkl_free(vector_int);
     vector_int=NULL;
     mkl_free(random_stream1);
     random_stream1=NULL;
     mkl_free(RechazX_dist);
     RechazX_dist=NULL;
     mkl_free(RechazX_dist_value);
     RechazX_dist_value=NULL;
     mkl_free(counterR_matrix);
     counterR_matrix=NULL;
     if (id != 0) {
	mkl_free(XmatC); 
	XmatC=NULL;
     }
     mkl_free(aux_distX_new);
     aux_distX_new=NULL;
     mkl_free(vectorX); 
     vectorX=NULL;
     mkl_free(vectorY);
     vectorY=NULL;
     mkl_free(Rstar);
     Rstar=NULL;
     mkl_free(distX);
     distX=NULL;
     mkl_free(distY);
     distY=NULL;
     mkl_free(R_aux);
     R_aux=NULL;

}


