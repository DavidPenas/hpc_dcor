#include <stdio.h>
#include <stdlib.h>
#include <Rinternals.h>
#include <mkl.h>
#include <mkl_cblas.h>
#include <mkl_lapack.h>
#include <immintrin.h>

typedef struct {
     int n_size;
     int n_size2;
     int n_size_real;
     int n_size_real2;
     int n_size_real8;
     int n_size_real28;
     float inv_n_size;
     float inv_n_size2;
     float neg_n_size;
} commonvars;

void testdcor(int, int, SEXP , SEXP , SEXP , SEXP , SEXP , SEXP , SEXP , SEXP , int *,  VSLStreamStatePtr *, SEXP *);
