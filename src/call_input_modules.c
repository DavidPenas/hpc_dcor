#include <stdlib.h>
#include <Rinternals.h>
#include <string.h>
#include <Rembedded.h>
#include <Rdefines.h>
#include "program_data.h"

void load_source(const char *name)
{
    SEXP e;
 
    PROTECT(e = lang2(install("source"), mkString(name)));
    R_tryEval(e, R_GlobalEnv, NULL);
    UNPROTECT(1);
}

void init_R(){
    int r_argc = 4;
    char *r_argv[] = { "R", "--vanilla", "--silent", "--slave" };
    Rf_initEmbeddedR(r_argc, r_argv);

}

void close_R(int counterp){
    UNPROTECT(counterp);
    Rf_endEmbeddedR(0);    
}

void ReadFile_input_file(const char *pathR, const char *file, program_data *program) {
    SEXP result_R, input_arg, root_arg1, root_arg2;
    int errorOccurred;
    int *void_parameter;
    char cmd1[200], cmd2[200]; 

    load_source(pathR);    
    void_parameter = (int *) malloc(sizeof(int));	
    void_parameter[0]=program->option;

    PROTECT(input_arg = allocVector(INTSXP, 1));
    program->counterp++;

    memcpy(INTEGER(input_arg), void_parameter, sizeof(int));			
     
    PROTECT(root_arg1 = allocVector(STRSXP, 1));
    program->counterp++;

    sprintf (cmd1, "%s", program->rootX);
    SET_STRING_ELT(root_arg1, 0, mkChar(cmd1));

    PROTECT(root_arg2 = allocVector(STRSXP, 1));
    program->counterp++;

    sprintf (cmd2, "%s", program->rootY);
    SET_STRING_ELT(root_arg2, 0, mkChar(cmd2));

    PROTECT(result_R = lang4(install(file), input_arg, root_arg1, root_arg2));
    program->counterp++;

    PROTECT( program->input_data = R_tryEval(result_R, R_GlobalEnv, &errorOccurred));
    program->counterp++;

    if (!errorOccurred){
        program->p       =VECTOR_ELT(program->input_data,  0);
        program->distance=VECTOR_ELT(program->input_data,  3);
        program->N       =VECTOR_ELT(program->input_data,  5);
        program->seed    =VECTOR_ELT(program->input_data,  7);
        program->silent  =VECTOR_ELT(program->input_data,  9);

    } else {
        perror("ERROR IN ReadFile_input_module\n");
        close_R(program->counterp);
        exit(-1);
    }

    free(void_parameter);
    void_parameter=NULL;
}


void ReadFile_input_module(const char *pathR, const char *file,  program_data *program) {
    SEXP result_R;
    int errorOccurred;

    load_source(pathR);

    PROTECT(result_R = lang2(install(file), program->input_data));
    program->counterp++;

    PROTECT( program->input_module = R_tryEval(result_R, R_GlobalEnv, &errorOccurred));
    program->counterp++;

    if (!errorOccurred){
        program->Xmat	=VECTOR_ELT(program->input_module, 0);
	program->Ymat	=VECTOR_ELT(program->input_module, 1);
	program->n1	=VECTOR_ELT(program->input_module, 2);
	program->n2	=VECTOR_ELT(program->input_module, 3);
	program->B1	=VECTOR_ELT(program->input_module, 4);
	program->B2	=VECTOR_ELT(program->input_module, 5);
	program->alpha	=VECTOR_ELT(program->input_module, 6);
    } else {
	perror("ERROR IN ReadFile_input_module\n");
        close_R(program->counterp);
        exit(-1);
    }
}



void ReadFile_processing_results(const char *pathR, const char *file,  program_data *program) {
    SEXP result_R;
    int errorOccurred;

    load_source(pathR);

    if (program->option==0) {
	program->RechazY = PROTECT(allocMatrix(REALSXP, 1, 1));
	program->counterp++;
    } 
    else if (program->option==1) {
        program->RechazX = PROTECT(allocMatrix(REALSXP, 1, 1));
	program->counterp++;
    }
    else if (program->option==2) {
        program->RechazY = PROTECT(allocMatrix(REALSXP, 1, 1));
        program->counterp++;
        program->RechazX = PROTECT(allocMatrix(REALSXP, 1, 1));
        program->counterp++;
	
    }

    PROTECT(result_R = lang5(install(file), program->RechazX, program->RechazY, program->input_module, program->input_data));
    program->counterp++;

    PROTECT( program->result = R_tryEval(result_R, R_GlobalEnv, &errorOccurred));
    program->counterp++;

    if (errorOccurred){
        perror("ERROR IN ReadFile_processing_results\n");
	close_R(program->counterp);
	exit(-1);
    }
}

void ReadFile_write_files(const char *pathR, const char *file,  program_data *program) {
    SEXP result_R, S;
    int errorOccurred;

    load_source(pathR);

    PROTECT(result_R = lang4(install(file), program->result, program->input_data, program->input_module));
    program->counterp++;

    PROTECT( S = R_tryEval(result_R, R_GlobalEnv, &errorOccurred));
    program->counterp++;

    if (errorOccurred){
        perror("ERROR IN ReadFile_write_files\n");
        close_R(program->counterp);
        exit(-1);
    }
}

