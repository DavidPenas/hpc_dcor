#include <stdio.h>
#include <mpi.h> 
#include "program_data.h"
#include "call_input_modules.h"
#include "cdcordist.h"
#include <stdlib.h>
#include <mkl_vsl.h>

int main(int argc, char** argv) {
	int NPROC, id;
        SEXP input_results;
        VSLStreamStatePtr str;
        double seedC;
        program_data *data;

        data = (program_data *) malloc(sizeof(program_data));

	// INITIALIZE MPI
        MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &NPROC);
	MPI_Comm_rank(MPI_COMM_WORLD, &id); 



	if (id == 0) {
		printf("NUMBER OF PROCESSORS: %d\n", NPROC);
		if(argc<2) {
			printf("ERROR: format->    ./bin/program option (1,2 or 3)\n"); 
			exit(1);
		}
		data->option=strtol(argv[1], NULL, 10);

                if (data->option == 0) printf("option: processing matrix_X\n");
                if (data->option == 1) printf("option: processing matrix_Y\n");
                if (data->option == 2) {
			printf("option: processing results\n");
			if(argc!=4) { 
				printf("ERROR: format->    ./bin/program 3 rootX rootY\n");
				exit(1);
			} else { 
				printf("root Matrix_X : %s\n", argv[2]);
                        	printf("root Matrix_Y : %s\n", argv[3]);
				data->rootX=argv[2];
				data->rootY=argv[3];
			}
		} else {
                                data->rootX=" ";
                                data->rootY=" ";
		}
	}
        MPI_Barrier(MPI_COMM_WORLD);
	
	if (id == 0) {
		// Starting R session
		init_R();
		data->counterp = 0;
		// Read input file	
		ReadFile_input_file("R/input/input_file.R", "input_file", data);
	}

        MPI_Bcast(&(data->option),1,MPI_INT,0,MPI_COMM_WORLD);
	
	// Initialize random stream
	initialize_random_streams(NPROC,id,&str,data->seed);

        if (id == 0) {
		// Read input module
		ReadFile_input_module("R/method/input_module.R", "input_module", data);	
	}

	// Start the test
	if (data->option == 0)
        	testdcor(NPROC, id, data->Xmat, data->n1, data->p, data->silent, data->distance, data->B1, data->N, data->alpha, &(data->counterp), &str, &(data->RechazX) );
	if (data->option == 1) 
		testdcor(NPROC, id, data->Ymat, data->n2, data->p, data->silent, data->distance, data->B2, data->N, data->alpha, &(data->counterp), &str, &(data->RechazY) );

	// Processing the results obtained in the test stage.
        if (id == 0) {
		ReadFile_processing_results("R/method/processing_results.R", "processing_results",  data);
        	ReadFile_write_files("R/method/write_files.R", "write_files",  data);
	
		printf("CLOSE R Session\n");
		// Close R session
		close_R(data->counterp);
	}

#ifndef DETERMINISTIC
        // Deallocate random stream	
        vslDeleteStream( &str );
#endif
	// DEALLOCATE MEMORY
        MPI_Finalize();
	
	free(data);
	data=NULL;

	return (EXIT_SUCCESS);
}
