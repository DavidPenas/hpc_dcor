#include <mkl_vsl.h>
#include <Rinternals.h>
#include <mpi.h>
void initialize_random_streams(int NPROC, int id, VSLStreamStatePtr *str, SEXP seed){
	double seedC;
	if (NPROC == 1 ) {
        	// Initialize random stream
             	seedC = asReal(seed);
        	vslNewStream(str,VSL_BRNG_MT19937,seedC);
	} else {
		if (id == 0) { seedC = asReal(seed); }
		MPI_Bcast(&seedC,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
                seedC = seedC * (id+1);
		vslNewStream(str,VSL_BRNG_MT19937,seedC);
	}
}
