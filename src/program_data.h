#include <Rinternals.h>

typedef struct {
	SEXP input_data;
	SEXP input_module;
        SEXP RechazX;
	SEXP RechazY;
	SEXP result;
	int counterp;
	// input_data
	SEXP silent;
	SEXP seed;
	SEXP N;
	SEXP p;
	SEXP distance;
	// input_odule
	SEXP  Xmat;
	SEXP  Ymat;
	SEXP  n1;
	SEXP  n2;
	SEXP  B1;
	SEXP  B2;
	SEXP  alpha;
	SEXP  tvec;
	int option;
	const char *rootX;
	const char *rootY;	
} program_data;
