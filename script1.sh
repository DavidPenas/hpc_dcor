#!/bin/bash
#SBATCH -t 05:00:00
#SBATCH -n 192
#SBATCH --error=ERROR1
#SBATCH -p thinnodes
module load intel/2016.4.258
module load imkl openmpi
module load bzip2/1.0.6
module load xz/5.2.4
module load pcre/8.42
module load curl/7.61.1 

HOME_DCOR=/home/usc/ei/drp/HPC_DCOR/hpc_dcor_v1
R_HOME=~/R/lib64/R
LD_LIBRARY_PATH=/home/usc/ei/drp/R/lib64/R/lib:$LD_LIBRARY_PATH

cd $HOME_DCOR

export HOME_DCOR
export R_HOME
export LD_LIBRARY_PATH

srun ./bin/program 0
